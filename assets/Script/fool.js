// Learn cc.Class:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/class.html
//  - [English] http://docs.cocos2d-x.org/creator/manual/en/scripting/class.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://docs.cocos2d-x.org/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] https://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

cc.Class({
    extends: cc.Component,

    properties: {
        currentX: -240,
        // foo: {
        //     // ATTRIBUTES:
        //     default: null,        // The default value will be used only when the component attaching
        //                           // to a node for the first time
        //     type: cc.SpriteFrame, // optional, default is typeof default
        //     serializable: true,   // optional, default is true
        // },
        // bar: {
        //     get () {
        //         return this._bar;
        //     },
        //     set (value) {
        //         this._bar = value;
        //     }
        // },
    },

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {},

    start () {

        this.node.zIndex=10;
        console.log();
        
    },
    update (dt) {
        
        var gameState=this.node.parent.getComponent("main").gameState;
        if(gameState==1)
        {
            if(this.node.x<=-820)
            {
                this.node.x=-240;
            }
            // else
            // {
            //     this.node.x=this.node.x-2;
            // }
        }
        else
        {
            let rbody = this.node.getComponent(cc.RigidBody);
            var vel=rbody.linearVelocity;
            vel.x=0;
            vel.y=0;
            rbody.linearVelocity=vel;
        }
       
    },


});
