// Learn cc.Class:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/class.html
//  - [English] http://docs.cocos2d-x.org/creator/manual/en/scripting/class.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://docs.cocos2d-x.org/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] https://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

cc.Class({
    extends: cc.Component,

    properties: {
        score: {
            default: null,
            type: cc.Node
        }
        // foo: {
        //     // ATTRIBUTES:
        //     default: null,        // The default value will be used only when the component attaching
        //                           // to a node for the first time
        //     type: cc.SpriteFrame, // optional, default is typeof default
        //     serializable: true,   // optional, default is true
        // },
        // bar: {
        //     get () {
        //         return this._bar;
        //     },
        //     set (value) {
        //         this._bar = value;
        //     }
        // },
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        this.node.zIndex=10;
        this.score.zIndex=11;
    },

    start () {

    },
    onDestroy(){    
        
    },
    // update (dt) {},

        // 只在两个碰撞体开始接触时被调用一次
    onBeginContact: function (contact, selfCollider, otherCollider) {
            
            console.log(otherCollider.node);
            console.log("开始发生碰撞");
            var name=otherCollider.node.name;
            console.log(name);
            if(name=="emptyNode")
            {
                console.log("emptyNode");
                var game=this.node.parent.getComponent("main");
                game.score=game.score+1;
                console.log(game.score);
                var label=this.score.getComponent(cc.Label);
                console.log(label);
                label.string=game.score;
            }
            else if(name!="top")
            {

                var game=this.node.parent.getComponent("main");
                game.gameOver();
            }
            
           

            // if (otherCollider.node.group == "floor") {
            //     if (selfCollider.node.y > otherCollider.node.y) {
            //         console.log("开始重置跳跃");
            //         this.isJump = false;
            //         this.isJumpSecond = false;
            //     }
            //     this.floorRotation = otherCollider.node.angle;
            //     console.log("赋值" + this.floorRotation);
            //     if (this.floorRotation == 0) {
            //         this.rBody.gravityScale = 5;
            //         console.log(this.floorRotation);
            //     }
            //     else {
            //         this.rBody.gravityScale = 0;
            //         console.log(this.floorRotation);
            //     }
    
            // }

        }

});
