// Learn cc.Class:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/class.html
//  - [English] http://docs.cocos2d-x.org/creator/manual/en/scripting/class.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://docs.cocos2d-x.org/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] https://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

cc.Class({
    extends: cc.Component,

    properties: {
        prefabPipeGroup: cc.Prefab,
        gameState: 0,
        score:0,
        bird: {
            default: null,
            type: cc.Node
        },
        btnStart: {
            default: null,
            type: cc.Node
        },
        md5:null
        // foo: {
        //     // ATTRIBUTES:
        //     default: null,        // The default value will be used only when the component attaching
        //                           // to a node for the first time
        //     type: cc.SpriteFrame, // optional, default is typeof default
        //     serializable: true,   // optional, default is true
        // },
        // bar: {
        //     get () {
        //         return this._bar;
        //     },
        //     set (value) {
        //         this._bar = value;
        //     }
        // },
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad() {
        
        this.md5=require("md5");
        // cc.View.setFrameSize(480, 640);
        //启动物理
        cc.director.getPhysicsManager().enabled = true;
        //启动碰撞
        cc.director.getCollisionManager().enabled = true;
        this.node.on(cc.Node.EventType.MOUSE_DOWN, this.clickFunction, this);
        this.node.on(cc.Node.EventType.TOUCH_START, this.clickFunction, this);
        this.rBody = this.bird.getComponent(cc.RigidBody);        
        this.animation = this.bird.getComponent(cc.Animation);
    },

    start() {
        this.startGame();
        
    },
    onDestroy() {
        clearInterval(this.si);
        this.node.off(cc.SystemEvent.EventType.MOUSE_DOWN, this.clickFunction, this);
        this.node.off(cc.SystemEvent.EventType.TOUCH_START, this.clickFunction, this);
    },
    createPipeGroup() {
        var pipeGroup = cc.instantiate(this.prefabPipeGroup);
        pipeGroup.x = 500;
        var zf = Math.random();
        console.log(zf);
        if (zf > 0.5) {
            if (zf > 0.8) {
                pipeGroup.y = parseInt(150 + Math.random() * 30);
            }
            else {
                pipeGroup.y = parseInt(Math.random() * 180);
            }

        }
        else {
            pipeGroup.y = - parseInt(Math.random() * 85);
        }
        
        this.node.addChild(pipeGroup);
    },
    //开始游戏
    startGame() {
        this.gameState = 1;
        var that = this;
        this.si = setInterval(function () {
            that.createPipeGroup();
        }, 1200);        
         this.btnStart.active=false;
        console.log(this.btnStart);
        //设置

    },
    //游戏结束
    gameOver() {
        if(this.gameState==1)
        {
            console.log("game over");
            this.gameState = 0;
            clearInterval(this.si);
            this.animation.stop();
            var v = this.rBody.linearVelocity;
            v.y = 0;
            v.x=0;
            this.rBody.linearVelocity = v;
            this.btnStart.active=true;
            this.httpPost();
        }
    },
    clickFunction() {
        if(this.gameState==1)
        {
            var v = this.rBody.linearVelocity;
            v.y = 600;
            this.rBody.linearVelocity = v;
        }
        
    },
    httpPost() {
        console.log("test1");
            var xhr = cc.loader.getXMLHttpRequest();
            xhr.onreadystatechange = function () {
                console.log('xhr.readyState='+xhr.readyState+'  xhr.status='+xhr.status);                
                if (xhr.readyState === 4 && (xhr.status >= 200 && xhr.status < 300)) {
                    var respone = xhr.responseText;
                    console.log(respone);
                    //resolve(respone);
                }
            };
            var url_temp = "https://api.wangbudong.com/ScoreAPI.ashx"; //这里如果打包成app则不可用相对路径
            xhr.open("POST", url_temp, true);
            // note: In Internet Explorer, the timeout property may be set only after calling the open()
            // method and before calling the send() method.
            xhr.timeout = 5000; // 5 seconds for timeout
            xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
            let signStr = "4BE60292-3AE0-47AB-A7AA-AF19BB1CA980&flappyBird&" + this.score + "&907E17014A6949EF8F1CCD689C223BA5";
            let sign = this.md5.hex_md5(signStr);
            xhr.send("gameId=4BE60292-3AE0-47AB-A7AA-AF19BB1CA980&gameName=flappyBird&score=" + this.score + "&sign=" + sign);
    },
    update (dt) {        
        if(this.gameState==1)
        {
            var v = this.rBody.linearVelocity;
            if(v.y>0)
            {
                this.bird.angle=30;
            }
            else
            {            
                this.bird.angle=-30;
            }
        }       
        if(this.gameState==0)
        {
            //console.log(v);
            // var v = this.rBody.linearVelocity;
            // v.y = 0;
            // v.x=0;
            // this.rBody.linearVelocity = v;
        }
        

    },
});
