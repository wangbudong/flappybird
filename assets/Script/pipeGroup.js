// Learn cc.Class:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/class.html
//  - [English] http://docs.cocos2d-x.org/creator/manual/en/scripting/class.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://docs.cocos2d-x.org/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] https://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

cc.Class({
    extends: cc.Component,

    properties: {
        // foo: {
        //     // ATTRIBUTES:
        //     default: null,        // The default value will be used only when the component attaching
        //                           // to a node for the first time
        //     type: cc.SpriteFrame, // optional, default is typeof default
        //     serializable: true,   // optional, default is true
        // },
        // bar: {
        //     get () {
        //         return this._bar;
        //     },
        //     set (value) {
        //         this._bar = value;
        //     }
        // },
    },

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {},

    start() {

    },

    update(dt) {
        var gameState = this.node.parent.getComponent("main").gameState;
        if (gameState == 1) {           
            var childs = this.node.children;
            var child = childs[0];
            if (child.x >= -1000) {
                //    this.node.x=this.node.x-2;
                //    console.log(this.node.x);
                //        var childs=this.node.children;
                //        childs.forEach(element => {
                //            element.x=0;
                //        });
            }
            else {
                console.log("发生销毁");
                this.node.removeFromParent();
            }

            // if(this.node.x>=-500)
            // {
            // //    this.node.x=this.node.x-2;
            // //    console.log(this.node.x);
            // //        var childs=this.node.children;
            // //        childs.forEach(element => {
            // //            element.x=0;
            // //        });
            // }
            // else
            // {
            //    this.node.removeFromParent();
            // }
        }
        else {
            this.rBody = this.node.getComponent(cc.RigidBody);
            let childs = this.node.children;
            childs.forEach(element => {
                let rbody = element.getComponent(cc.RigidBody);
               
                if(rbody.linearVelocity!=null&&rbody.linearVelocity.x!=0)
                {
                    console.log(rbody);
                    let vel=rbody.linearVelocity;
                    vel.x=0;
                    rbody.linearVelocity = vel;                    
                }
               
            });



        }
    }
});
